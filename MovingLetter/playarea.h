#ifndef PLAYAREA_H
#define PLAYAREA_H
#define MAGIC_NUMBER 3
#define MINIMUM_SIZE 11


//------------------------------------------------------
// This class draws the playfield and outputs player
// position, stored in Player class


class PlayArea
{

    int size;
    char** area;


public:

    int getSize();
    void setSize( int value );

//  function injecting position to playArea

    void insertPosition( int valueX, int valueY );

// Clear position on the play field
    void clearPosition( int valueX, int valueY );


// Playfield size change
    void changeSize();


//  drawing area

    void drawArea();

// default constructor

    PlayArea();

//  Variable constructor

    PlayArea( int size);

// method to initialize for constructor
//

    void initialize( int size );


// Destructor

    ~PlayArea();

};


#endif // PLAYAREA_H

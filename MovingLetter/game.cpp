#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <fstream>

#include "game.h"

#define AREA_SIZE 9
#define MINIMUM_SIZE 11
#define MAXIMUM_SIZE 101
#define MAGIC_NUMBER 3

using namespace std;

Game::Game():action('f'), size(MINIMUM_SIZE), gameOn(true), tempX(MAGIC_NUMBER), tempY(MAGIC_NUMBER), field(new PlayArea(getSize()))
{}

Game::~Game()
{

    delete field;

}

void Game::addPlayer(Player *a_pNewPlayer)
{
    m_pPlayer = a_pNewPlayer;
}

char Game::getAction()
{
    return action;
}

void Game::setAction(char value)
{
    action = value;
}

Game::getSize()
{
    return size;
}

void Game::setSize(int value)
{
    size = value;
}

bool Game::getGameOn()
{
    return gameOn;
}

void Game::setGameOn(bool value)
{
    gameOn = value;
}

Game::getTempX()
{
    return tempX;
}

Game::getTempY()
{
    return tempY;
}

void Game::setTemp(int valueX, int valueY)
{

    tempX = valueX;
    tempY = valueY;

}

void Game::gameIO()
{

    cout << endl << endl << "Aby poruszyc sie literka, wcisnij odpowiedni klawisz i zatwierdz enter: " << endl;
    cout << "w - do gory" << endl;
    cout << "s - w dol" << endl;
    cout << "a - w lewo" << endl;
    cout << "d - w prawo" << endl;
    cout << "Nacisnij 0 aby wyjsc" << endl;
    cout << "Nacisnij 1 aby zapisac" << endl;
    cout << "Nacisnij 2 aby wczytac" << endl;
    cout << "Nacisnij 9 aby zmienic wielkosc planszy" << endl;

    cin >> action;

    switch( action )
    {

    case 'w':
        setAction(action);
        break;

    case 's':
        setAction(action);
        break;

    case 'a':
        setAction(action);
        break;

    case 'd':
        setAction(action);
        break;

    case '0':
        gameOn = false;
        break;

    case '1':
        saveGame();
        break;

    case '2':
        loadGame();
        break;

    case '9':
        field->changeSize();
        break;

    default:
        cout << endl << "Podano zla wartosc" << endl;
        system("pause");
        break;

    }

}


void Game::gameplay()
{

    while(getGameOn() == true)
    {
        setTemp(m_pPlayer->getPositionX(), m_pPlayer->getPositionY());
        field->insertPosition(m_pPlayer->getPositionX(), m_pPlayer->getPositionY());
        field->drawArea();
        field->clearPosition(getTempX(), getTempY());
        gameIO();
        m_pPlayer->playerMove(getAction(), field->getSize());
        system("cls");
    }
}

void Game::saveGame()
{

    ofstream out( "save.dat", ios :: out | ios :: binary);
    if(out.is_open() == true)
    {
        cout << endl << "Plik utworzony" << endl;

        out << m_pPlayer->getPositionX() << " ";
        out << m_pPlayer->getPositionY() << " ";
        out << field->getSize();


        out.close();
        cout << endl << "Zapisano" << endl << endl;
        system("pause");

    }else cout << endl << "Nie udalo sie otworzyc pliku" << endl;

}

void Game::loadGame()
{
    cout << endl << "Odczyt pliku" << endl;
    int buffX;
    int buffY;
    int buffS;

    ifstream in("save.dat", ios::in | ios::binary );
    if( in.is_open() == true )
    {
        in >> buffX >> buffY >> buffS;
        in.close();
        cout << endl << "Wczytano poprawnie" << endl;

    }else cout << endl << "Nie udalo sie otworzyc pliku" << endl;

    m_pPlayer->setPositionX(buffX);
    m_pPlayer->setPositionY(buffY);
    field->setSize(buffS);
    field->initialize(buffS);
    system("pause");
}


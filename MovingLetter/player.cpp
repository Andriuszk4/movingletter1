#include <iostream>
#include "player.h"

using namespace std;

Player::Player():positionX(MAGIC_NUMBER), positionY(MAGIC_NUMBER), playerSymbol('o')
{
}

Player::~Player()
{}

void Player::playerMove( char action, int size )
{
    switch( action )
    {
    case 's':
    positionX++;
    if( positionX > size - 1 )
    {positionX = 0;}
        break;

    case 'w':
    positionX--;
    if( positionX < 0 )
    { positionX = ( size - 1 ); }
        break;

    case 'a':
    positionY = positionY - 2;
    if( positionY < 0 )
    { positionY =( size - 2 );}
        break;

    case 'd':
    positionY = positionY + 2;
    if( positionY > size - 1 )
    { positionY = 1; }
        break;

    default:
        break;

    }
}

Player::getPositionX()
{
    return positionX;
}

void Player::setPositionX(int value)
{
    positionX = value;
}

Player::getPositionY()
{
    return positionY;
}

void Player::setPositionY(int value)
{
    positionY = value;
}


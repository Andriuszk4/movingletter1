#ifndef MLINTERFACE_H
#define MLINTERFACE_H
#define MAGIC_NUMBER 3
#define MINIMUM_SIZE 11

#include "playarea.h"
#include "player.h"


// This class is responsible for main game interface.
// Handles player I/O, and asks player class for movement.

class Game
{

    char action;
    int size;
    bool gameOn;
    int tempX;
    int tempY;
    PlayArea* field;
    Player* m_pPlayer;

public:

    Game();
    ~Game();

    void addPlayer(Player* a_pNewPlayer);

    char getAction();
    void setAction(char value);

    int getSize();
    void setSize( int value );

    bool getGameOn();
    void setGameOn( bool value );

    int getTempX();

    int getTempY();

    void setTemp( int valueX, int valueY );

    void gameIO();

    void gameplay();

    void saveGame();

    void loadGame();

};


#endif // MLINTERFACE_H

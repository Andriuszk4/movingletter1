#include <iostream>
#include "playarea.h"

#define AREA_SIZE 9
#define MINIMUM_SIZE 11
#define MAXIMUM_SIZE 101
#define MAGIC_NUMBER 3

//-----------------------------------------------------

using namespace std;

int PlayArea::getSize()
{
    return size;
}

void PlayArea::setSize(int value)
{
size = value;
}

void PlayArea::insertPosition(int valueX, int valueY)
{
    area[ valueX ][ valueY ] = 'O';
}

void PlayArea::clearPosition( int valueX, int valueY )
{
  area[ valueX ][ valueY ] = 'x';
}


void PlayArea::changeSize()
{
    for( int i = 0; i < size; i++)
    {
        delete[] area [i];
    }
    delete[] area;

    cout << "Podaj wielkosc planszy (od 5 do 101)" << endl;
    bool verify;
    do
    {
        cin >> size;
        verify = cin.good();
        cin.clear();
        cin.sync();
    }while( verify == 0 );

    initialize(size);
}

void PlayArea::drawArea()
{
    cout << endl << "Wielkosc tablicy: " << size << endl;
    for( int line = 0; line < size; ++line)
    {
        for( int column = 0; column < size; ++column)
        {
            cout << area[ line ][ column ] << " ";
        }
        cout << endl;
    }
}


void PlayArea::initialize(int size)
{

//  area size needs to be an odd number. If player inputs an even number,
//  the constructor will switch it to a number bigger by one

    if( size < MINIMUM_SIZE)
        {
            size = MINIMUM_SIZE;
        }else if( size > MAXIMUM_SIZE)
        {
            size = MAXIMUM_SIZE;
        }

        if( size % 2 == 0)
        {
           this->size = size-1;
        }else
           this->size = size;

//   rest is just like in default
    area = new char* [ size ];

    for( int i = 0; i < size; i++)
    {
        area[ i ] = new char[ size ];
    }

    for( int line = 0; line < size; line ++)
    {
        for( int column = 0; column < size; column ++)
        {
            if( column %2 == 0)
                area[ line ][ column ] = '|';
            else
                area[ line ][ column ] = 'x';
        }
    }

}

PlayArea::PlayArea(int size):size(MINIMUM_SIZE)
{

    initialize(size);

}


PlayArea::~PlayArea()
{

    for( int i = 0; i < size; i++)
    {
        delete[] area [i];
    }
    delete[] area;

}

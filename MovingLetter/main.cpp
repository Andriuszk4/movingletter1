#include <iostream>
#include <windows.h>
#include <cstdlib>
#include "game.h"
#include "playarea.h"
#include "player.h"

#define AREA_SIZE 9
#define MINIMUM_SIZE 11
#define MAXIMUM_SIZE 101

using namespace std;


int main()
{

    Game *game = new Game;
    Player *player1 = new Player;


    game->addPlayer(player1);
    game->gameplay();


    delete game;
    delete player1;

    return 0;
}

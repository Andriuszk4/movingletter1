#ifndef PLAYER_H
#define PLAYER_H

#define MAGIC_NUMBER 3

//-------------------------------------------------------
//  This class is responsible for player position,
//  representation of him on field and player movement

class Player
{

    int positionX;
    int positionY;
    char playerSymbol;

public:


    Player();
    ~Player();

    void playerMove(char action, int size);

    int getPositionX();
    void setPositionX( int value );

    int getPositionY();
    void setPositionY( int value );

};

#endif // PLAYER_H

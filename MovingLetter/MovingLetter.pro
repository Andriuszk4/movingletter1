TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    playarea.cpp \
    player.cpp \
    game.cpp

HEADERS += \
    playarea.h \
    player.h \
    game.h
